# tagol

TAGOL (Test A GO Lambda) is a small DSL to test an AWS API Gateway-proxified AWS Lambda implemented in Go.

## Usage

See the [generated Go doc](https://pkg.go.dev/gitlab.com/mwwaa/tagol.git?tab=doc).
Below, a short example :

```
    // main.go
	func handleRequest(request events.APIGatewayProxyRequest) (response events.APIGatewayProxyResponse, err error) {
		return
	}

    // main_test.go
    func TestHttpOk(t *testing.T) {
        tagol.Invoking(handleRequest).
            Should(t).
            ReturnNoError().
            ReturnStatus(http.StatusOK)
    }
```

To make dynamic assertions on the Lambda's return body you can combine 

- `ReturnValidatedBodyAs`
- The [validator library](https://github.com/go-playground/validator)

Just pass the `validate.Struct` method as `ReturnValidatedBodyAs`'s second argument, and write your struct's validation tags.

## License

Provided under the [MIT License](./LICENSE).
