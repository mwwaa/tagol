package tagol

import (
	"encoding/json"
	"errors"
	"fmt"
	"github.com/aws/aws-lambda-go/events"
	"net/http"
	"reflect"
	"testing"
)

type mockedFatalfer struct {
	fail      bool
	message   string
	arguments []interface{}
}

func (m *mockedFatalfer) Fatalf(message string, arguments ...interface{}) {
	m.message = message
	m.fail = true
	m.arguments = arguments
}

func (m mockedFatalfer) failed(t *testing.T) {
	if !m.fail {
		t.Fatal("Assertions should have failed but passed")
	}
}

func (m mockedFatalfer) passed(t *testing.T) {
	if m.fail {
		t.Fatalf("Assertions should have passed, but failed with :\n%v\n", m.fatalMessage())
	}
}

func (m mockedFatalfer) fatalMessage() string { return fmt.Sprintf(m.message, m.arguments...) }

type testingType struct {
	Name string
	Area int
}

func TestNoErrorFailsWhenError(t *testing.T) {
	tester := new(mockedFatalfer)
	fut := func(request events.APIGatewayProxyRequest) (response events.APIGatewayProxyResponse, err error) {
		err = errors.New("testing error")
		return
	}

	Invoking(fut).Should(tester).ReturnNoError()

	tester.failed(t)
}

func TestNoErrorPassWhenNoError(t *testing.T) {
	tester := new(mockedFatalfer)
	fut := func(request events.APIGatewayProxyRequest) (response events.APIGatewayProxyResponse, err error) {
		return
	}

	Invoking(fut).Should(tester).ReturnNoError()

	tester.passed(t)
}

func TestErrorFailsWhenNoError(t *testing.T) {
	tester := new(mockedFatalfer)
	fut := func(request events.APIGatewayProxyRequest) (response events.APIGatewayProxyResponse, err error) {
		return
	}

	Invoking(fut).Should(tester).ReturnError("testing error")

	tester.failed(t)
}

func TestErrorPassWhenError(t *testing.T) {
	tester := new(mockedFatalfer)
	fut := func(request events.APIGatewayProxyRequest) (response events.APIGatewayProxyResponse, err error) {
		err = errors.New("testing error")
		return
	}

	Invoking(fut).Should(tester).ReturnError("testing error")

	tester.passed(t)
}

func TestStatusFailsWhenNotEqual(t *testing.T) {
	tester := new(mockedFatalfer)
	fut := func(request events.APIGatewayProxyRequest) (response events.APIGatewayProxyResponse, err error) {
		response.StatusCode = http.StatusInternalServerError
		return
	}

	Invoking(fut).Should(tester).ReturnStatus(http.StatusOK)

	tester.failed(t)
}

func TestStatusPassWhenEqual(t *testing.T) {
	tester := new(mockedFatalfer)
	fut := func(request events.APIGatewayProxyRequest) (response events.APIGatewayProxyResponse, err error) {
		response.StatusCode = http.StatusOK
		return
	}

	Invoking(fut).Should(tester).ReturnStatus(http.StatusOK)

	tester.passed(t)
}

func TestBodyFailsWhenNotEqual(t *testing.T) {
	respBody := testingType{Name: "Gopher", Area: 51}
	tester := new(mockedFatalfer)
	fut := func(request events.APIGatewayProxyRequest) (response events.APIGatewayProxyResponse, err error) {
		raw, err := json.Marshal(respBody)
		response.Body = string(raw)
		return
	}

	Invoking(fut).Should(tester).ReturnBodyAs(new(testingType), &testingType{Name: "Arthur", Area: 42})

	tester.failed(t)
}

func TestBodyFailsWhenNotAsPointer(t *testing.T) {
	respBody := testingType{Name: "Gopher", Area: 51}
	tester := new(mockedFatalfer)
	fut := func(request events.APIGatewayProxyRequest) (response events.APIGatewayProxyResponse, err error) {
		raw, err := json.Marshal(respBody)
		response.Body = string(raw)
		return
	}

	Invoking(fut).
		Should(tester).
		ReturnBodyAs(testingType{Name: "Gopher", Area: 51}, &testingType{Name: "Arthur", Area: 42})

	tester.failed(t)
}

func TestBodyFailsWhenNotPointer(t *testing.T) {
	respBody := testingType{Name: "Gopher", Area: 51}
	tester := new(mockedFatalfer)
	fut := func(request events.APIGatewayProxyRequest) (response events.APIGatewayProxyResponse, err error) {
		raw, err := json.Marshal(respBody)
		response.Body = string(raw)
		return
	}

	Invoking(fut).Should(tester).ReturnBodyAs(new(testingType), testingType{Name: "Arthur", Area: 42})

	tester.failed(t)
}

func TestBodyPassWhenEqual(t *testing.T) {
	respBody := testingType{Name: "Gopher", Area: 51}
	tester := new(mockedFatalfer)
	fut := func(request events.APIGatewayProxyRequest) (response events.APIGatewayProxyResponse, err error) {
		raw, err := json.Marshal(respBody)
		response.Body = string(raw)
		return
	}

	Invoking(fut).Should(tester).ReturnBodyAs(new(testingType), &testingType{Name: "Gopher", Area: 51})

	tester.passed(t)
}

func TestValidateBodyFailsWhenNotAsPointer(t *testing.T) {
	respBody := testingType{Name: "Gopher", Area: 51}
	tester := new(mockedFatalfer)
	fut := func(request events.APIGatewayProxyRequest) (response events.APIGatewayProxyResponse, err error) {
		raw, err := json.Marshal(respBody)
		response.Body = string(raw)
		return
	}

	Invoking(fut).Should(tester).ReturnValidatedBodyAs(testingType{}, func(i interface{}) error { return nil })

	tester.failed(t)
}

func TestValidateBodyFailsWhenNilValidator(t *testing.T) {
	respBody := testingType{Name: "Gopher", Area: 51}
	tester := new(mockedFatalfer)
	fut := func(request events.APIGatewayProxyRequest) (response events.APIGatewayProxyResponse, err error) {
		raw, err := json.Marshal(respBody)
		response.Body = string(raw)
		return
	}

	Invoking(fut).Should(tester).ReturnValidatedBodyAs(new(testingType), nil)

	tester.failed(t)
}

func TestValidateBodyFailsWhenValidatorReturnsError(t *testing.T) {
	respBody := testingType{Name: "Gopher", Area: 51}
	tester := new(mockedFatalfer)
	fut := func(request events.APIGatewayProxyRequest) (response events.APIGatewayProxyResponse, err error) {
		raw, err := json.Marshal(respBody)
		response.Body = string(raw)
		return
	}

	Invoking(fut).Should(tester).ReturnValidatedBodyAs(new(testingType), func(i interface{}) error {
		return errors.New("testing error")
	})

	tester.failed(t)
}

func TestValidatedBodyFailsWhenValidatorReturnsError(t *testing.T) {
	respBody := testingType{Name: "Gopher", Area: 51}
	tester := new(mockedFatalfer)
	fut := func(request events.APIGatewayProxyRequest) (response events.APIGatewayProxyResponse, err error) {
		raw, err := json.Marshal(respBody)
		response.Body = string(raw)
		return
	}

	Invoking(fut).Should(tester).ReturnValidatedBodyAs(new(testingType), func(i interface{}) error {
		if body, ok := i.(*testingType); !ok {
			t.Fatalf("expected to validate type\ntestingType\n---\ngot\n%v", reflect.TypeOf(i))
		} else if body.Name != "Gopher" {
			t.Fatalf("expected to validate with Name\nGopher\n---\ngot\n%v", body.Name)
		} else if body.Area != 51 {
			t.Fatalf("expected to validate with Area\n51\n---\ngot\n%v", body.Area)
		}
		return errors.New("testing error")
	})

	tester.failed(t)
}

func TestValidatedBodyPassWhenValidatorReturnsNil(t *testing.T) {
	respBody := testingType{Name: "Gopher", Area: 51}
	tester := new(mockedFatalfer)
	fut := func(request events.APIGatewayProxyRequest) (response events.APIGatewayProxyResponse, err error) {
		raw, err := json.Marshal(respBody)
		response.Body = string(raw)
		return
	}

	Invoking(fut).Should(tester).ReturnValidatedBodyAs(new(testingType), func(i interface{}) error {
		if body, ok := i.(*testingType); !ok {
			t.Fatalf("expected to validate type\ntestingType\n---\ngot\n%v", reflect.TypeOf(i))
		} else if body.Name != "Gopher" {
			t.Fatalf("expected to validate with Name\nGopher\n---\ngot\n%v", body.Name)
		} else if body.Area != 51 {
			t.Fatalf("expected to validate with Area\n51\n---\ngot\n%v", body.Area)
		} else {
			return nil
		}
		return errors.New("testing error")
	})

	tester.passed(t)
}

func TestHeaderFailsWhenNone(t *testing.T) {
	tester := new(mockedFatalfer)
	fut := func(request events.APIGatewayProxyRequest) (response events.APIGatewayProxyResponse, err error) {
		return
	}

	Invoking(fut).Should(tester).ReturnHeader("Gopher-Name", "Goophy")

	tester.failed(t)
}

func TestHeaderFailsWhenNotEqual(t *testing.T) {
	tester := new(mockedFatalfer)
	fut := func(request events.APIGatewayProxyRequest) (response events.APIGatewayProxyResponse, err error) {
		response.Headers = map[string]string{"Gopher-Name": "Jon"}
		return
	}

	Invoking(fut).Should(tester).ReturnHeader("Gopher-Name", "Goophy")

	tester.failed(t)
}

func TestHeaderPassWhenEqual(t *testing.T) {
	tester := new(mockedFatalfer)
	fut := func(request events.APIGatewayProxyRequest) (response events.APIGatewayProxyResponse, err error) {
		response.Headers = map[string]string{"Gopher-Name": "Goophy"}
		return
	}

	Invoking(fut).Should(tester).ReturnHeader("Gopher-Name", "Goophy")

	tester.passed(t)
}

func TestFailsWhenOneAssertionFails(t *testing.T) {
	tester := new(mockedFatalfer)
	fut := func(request events.APIGatewayProxyRequest) (response events.APIGatewayProxyResponse, err error) {
		assertableMap(request.Headers).has("Warning", "High speed cars ahead", "header", t)
		assertableMap(request.QueryStringParameters).has("area", "gt42", "query", t)
		assertableMap(request.PathParameters).has("id", "a110", "path parameter", t)

		response.Headers = map[string]string{"Gopher-Name": "Goophy"}
		response.StatusCode = http.StatusOK
		return
	}

	// Act
	Invoking(fut).
		WithPathParameter("id", "a110").
		WithQuery("area", "gt42").
		WithHeader("Warning", "High speed cars ahead").
		WithBody(testingType{Name: "A", Area: 32}).

		// Assert
		Should(tester).
		ReturnNoError().
		ReturnHeader("Gopher-Name", "Goophy").
		ReturnBodyAs(new(testingType), &testingType{Name: "legend", Area: 110}).
		ReturnStatus(http.StatusOK)

	tester.failed(t)
}

func TestFailsWhenMultipleAssertionFails(t *testing.T) {
	tester := new(mockedFatalfer)
	fut := func(request events.APIGatewayProxyRequest) (response events.APIGatewayProxyResponse, err error) {
		assertableMap(request.Headers).has("Warning", "High speed cars ahead", "header", t)
		assertableMap(request.QueryStringParameters).has("area", "gt42", "query", t)
		assertableMap(request.PathParameters).has("id", "a110", "path parameter", t)

		response.Headers = map[string]string{"Gopher-Name": "Goophy"}
		response.StatusCode = http.StatusInternalServerError
		return
	}

	// Act
	Invoking(fut).
		WithPathParameter("id", "a110").
		WithQuery("area", "gt42").
		WithHeader("Warning", "High speed cars ahead").
		WithBody(testingType{Name: "A", Area: 32}).

		// Assert
		Should(tester).
		ReturnNoError().
		ReturnHeader("Gopher-Name", "Goophy").
		ReturnBodyAs(new(testingType), testingType{Name: "", Area: 0}).
		ReturnStatus(http.StatusOK)

	tester.failed(t)
}

func TestPassWhenMultipleAssertionPasses(t *testing.T) {
	tester := new(mockedFatalfer)
	fut := func(request events.APIGatewayProxyRequest) (response events.APIGatewayProxyResponse, err error) {
		assertableMap(request.Headers).has("Warning", "High speed cars ahead", "header", t)
		assertableMap(request.QueryStringParameters).has("area", "gt42", "query", t)
		assertableMap(request.PathParameters).has("id", "a110", "path parameter", t)

		raw, err := json.Marshal(testingType{Name: "legend", Area: 110})
		response.Body = string(raw)
		response.Headers = map[string]string{"Gopher-Name": "Goophy"}
		response.StatusCode = http.StatusOK
		return
	}

	// Act
	Invoking(fut).
		WithPathParameter("id", "a110").
		WithQuery("area", "gt42").
		WithHeader("Warning", "High speed cars ahead").
		WithBody(testingType{Name: "A", Area: 32}).

		// Assert
		Should(tester).
		ReturnNoError().
		ReturnHeader("Gopher-Name", "Goophy").
		ReturnBodyAs(new(testingType), &testingType{Name: "legend", Area: 110}).
		ReturnStatus(http.StatusOK)

	tester.passed(t)
}
