package tagol

import (
	"encoding/json"
	"github.com/aws/aws-lambda-go/events"
	"net/http"
	"testing"
)

func TestWithMethodHasMethod(t *testing.T) {
	// fut stands for "Function Under Test"
	fut := func(request events.APIGatewayProxyRequest) (response events.APIGatewayProxyResponse, err error) {
		if request.HTTPMethod != http.MethodOptions {
			t.Fatalf("expected method\n%v\n---\ngot\n%v", http.MethodOptions, request.HTTPMethod)
		}
		return
	}

	Invoking(fut).WithMethod(http.MethodOptions).Should(new(mockedFatalfer))
}

func TestWithBodyHasJson(t *testing.T) {
	body := testingType{Name: "Zjut", Area: 32}
	fut := func(request events.APIGatewayProxyRequest) (response events.APIGatewayProxyResponse, err error) {
		if expected, err := json.Marshal(body); string(expected) != request.Body || err != nil {
			t.Fatalf("expected body\n%v\n---\ngot\n%v", string(expected), request.Body)
		}
		return
	}

	Invoking(fut).WithBody(body).Should(new(mockedFatalfer))
}

func TestWithQueryHasQuery(t *testing.T) {
	fut := func(request events.APIGatewayProxyRequest) (response events.APIGatewayProxyResponse, err error) {
		assertableMap(request.QueryStringParameters).has("name", "Zjut", "query", t)
		return
	}

	Invoking(fut).WithQuery("name", "Zjut").Should(new(mockedFatalfer))
}

func TestWithPathHasPath(t *testing.T) {
	fut := func(request events.APIGatewayProxyRequest) (response events.APIGatewayProxyResponse, err error) {
		assertableMap(request.PathParameters).has("id", "d707ffd5e", "path parameter", t)
		return
	}

	Invoking(fut).WithPathParameter("id", "d707ffd5e").Should(new(mockedFatalfer))
}

func TestWithHeaderHasHeader(t *testing.T) {
	fut := func(request events.APIGatewayProxyRequest) (response events.APIGatewayProxyResponse, err error) {
		assertableMap(request.Headers).has("Warning", "Gophers bite", "header", t)
		return
	}

	Invoking(fut).WithHeader("Warning", "Gophers bite").Should(new(mockedFatalfer))
}

type assertableMap map[string]string

func (a assertableMap) has(key, expected, name string, t *testing.T) {
	if actual, ok := a[key]; !ok || actual != expected {
		t.Fatalf("expected %v\n%v: %v\n---\ngot\n%v (%v): %v", name, key, expected, key, ok, actual)
	}
}
