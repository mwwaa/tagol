// Package tagol (Test A GO Lambda) implements an AWS API Gateway-invoked Lambda function testing library.
// Tagol allows you to use a fluent, DSL-like interface to test your HTTP-handling AWS Lambda (using API Gateway proxy integration).
package tagol

import (
	"encoding/json"
	"github.com/aws/aws-lambda-go/events"
	"reflect"
)

// Invocation represents a call to the tested Lambda along with the testing request parameters
type Invocation struct {
	function func(events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error)
	req      events.APIGatewayProxyRequest
}

// Invoking returns an Invocation from the provided API Gateway-proxified Lambda function
func Invoking(function func(events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error)) Invocation {
	return Invocation{function: function}
}

// WithMethod sets the invocation's method
func (i Invocation) WithMethod(method string) Invocation {
	i.req.HTTPMethod = method
	return i
}

// WithBody sets the invocation's request body by marshalling the provided interface{} to JSON
func (i Invocation) WithBody(body interface{}) Invocation {
	serialized, err := json.Marshal(body)
	if err != nil {
		panic(err)
	}

	i.req.Body = string(serialized)

	return i
}

// WithQuery adds a query parameter to the invocation's request parameters
func (i Invocation) WithQuery(key, value string) Invocation {
	i.req.QueryStringParameters = fillOrCreateMap(i.req.QueryStringParameters, key, value)
	return i
}

// WithPathParameter adds a path parameter to the invocation's request parameters
func (i Invocation) WithPathParameter(key, value string) Invocation {
	i.req.PathParameters = fillOrCreateMap(i.req.PathParameters, key, value)
	return i
}

// WithHeader adds a header to the invocation's request parameters
func (i Invocation) WithHeader(key, value string) Invocation {
	i.req.Headers = fillOrCreateMap(i.req.Headers, key, value)
	return i
}

func fillOrCreateMap(aMap map[string]string, key, value string) map[string]string {
	if aMap != nil {
		aMap[key] = value
		return aMap
	}

	aMap = map[string]string{key: value}
	return aMap
}

type fatalfer interface {
	Fatalf(string, ...interface{})
}

// Assertions allows you to make assertions on the function's response and fails the test if an assertion is not met
type Assertions struct {
	resp  events.APIGatewayProxyResponse
	error error
	t     fatalfer
}

// Should takes a *testing.T or any value that provides a Fatalf(string, ...interface{}) method and returns an Assertions
func (i Invocation) Should(fatalfer fatalfer) Assertions {
	resp, err := i.function(i.req)
	return Assertions{resp: resp, error: err, t: fatalfer}
}

// ReturnNoError fails if the function returned an error
func (a Assertions) ReturnNoError() Assertions {
	if a.error != nil {
		a.t.Fatalf("expected error\nnil\n---\ngot\n%v", a.error)
	}

	return a
}

// ReturnError fails if the function did not returned an error that matches the provided string
func (a Assertions) ReturnError(expected string) {
	if a.error == nil || a.error.Error() != expected {
		a.t.Fatalf("expected error\n%v\n---\ngot\n%v", expected, a.error)
	}
}

// ReturnStatus fails if the function did not returned the expected HTTP status code
func (a Assertions) ReturnStatus(expected int) Assertions {
	if a.resp.StatusCode != expected {
		a.t.Fatalf("expected status\n%v\n---\ngot\n%v", expected, a.resp.StatusCode)
	}

	return a
}

// ReturnBodyAs fails if the function did not returned a JSON body that can be unmarshaled to the first (must be pointer) argument
// or if it's not equal to the second (must be pointer) argument
func (a Assertions) ReturnBodyAs(actual, expected interface{}) Assertions {
	a.unmarshalBody(actual)

	if !reflect.DeepEqual(expected, actual) {
		a.t.Fatalf("expected body to be\n%v\n(type %v)\n---\ngot\n%v\n(type %v)",
			expected, reflect.TypeOf(expected), actual, reflect.TypeOf(actual))
	}

	return a
}

// ReturnValidatedBodyAs fails if the function did not returned a JSON body that can be unmarshaled to the first (must be pointer) argument
// or if the provided validation function returns a non-nil error when called with the unmarshaled argument
func (a Assertions) ReturnValidatedBodyAs(actual interface{}, validator func(interface{}) error) Assertions {
	a.unmarshalBody(actual)

	if validator == nil {
		a.t.Fatalf("expected provided validator to be a func\n---\ngot\nnil")
		return a
	}

	if err := validator(actual); err != nil {
		a.t.Fatalf("expected body to be validated by the provided function\n---\ngot error\n%v", err)
	}

	return a
}

func (a Assertions) unmarshalBody(actual interface{}) {
	err := json.Unmarshal([]byte(a.resp.Body), actual)

	if err != nil {
		a.t.Fatalf("expected body to be unmarshaled to type\n%v\n---\ngot error %v", reflect.TypeOf(actual), err)
	}
}

// ReturnHeader fails if the function did not set the specified header
func (a Assertions) ReturnHeader(key, expected string) Assertions {
	if _, ok := a.resp.Headers[key]; !ok {
		a.t.Fatalf("expected header %v to be\n%v\n---\ngot\nnone", key, expected)
	}

	if actual := a.resp.Headers[key]; a.resp.Headers[key] != expected {
		a.t.Fatalf("expected header %v to be\n%v\n---\ngot\n%v", key, expected, actual)
	}

	return a
}
