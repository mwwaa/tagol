package tagol

import (
	"encoding/json"
	"fmt"
	"github.com/aws/aws-lambda-go/events"
	"net/http"
)

func Example() {
	tester := new(mockedFatalfer)
	fut := func(request events.APIGatewayProxyRequest) (response events.APIGatewayProxyResponse, err error) {
		raw, err := json.Marshal(testingType{Name: "legend", Area: 110})
		response.Body = string(raw)
		response.Headers = map[string]string{"Gopher-Name": "Goophy"}
		response.StatusCode = http.StatusOK
		return
	}

	// Act
	Invoking(fut).
		WithPathParameter("id", "a110").
		WithQuery("area", "gt42").
		WithHeader("Warning", "High speed cars ahead").
		WithBody(testingType{Name: "A", Area: 32}).

		// Assert
		Should(tester).
		ReturnNoError().
		ReturnHeader("Gopher-Name", "Goophy").
		ReturnValidatedBodyAs(new(testingType), func(_ interface{}) error { return nil }).
		ReturnStatus(http.StatusOK)

	fmt.Printf("Tests passed : %v", !tester.fail)
	// Output: Tests passed : true
}
